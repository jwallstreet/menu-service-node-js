var chai = require('chai');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var winston = require('winston');

describe('Routing', function () {
    var url = "localhost:3000/data/test";
    var testObjectID;

    before(function (done) {
        done();
    });

    describe('CRUD Testing', function () {
        it('Should successfully create a test object', function (done) {
            var testObject = [
                {
                    _id: "123456789012345678901234",
                    name: "testObjectFromUnitTest1",
                    description: "object created from unit test"
                },
                {
                    name: "testObjectFromUnitTest2",
                    description: "object created from unit test"
                },
                {
                    name: "testObjectFromUnitTest3",
                    description: "object created from unit test"
                },
                {
                    name: "testObjectFromUnitTest4",
                    description: "object created from unit test"
                },
                {
                    name: "testObjectFromUnitTest5",
                    description: "object created from unit test"
                },
            ]

            request(url)
                .post('')
                .send(testObject)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });


        it('Should return 400 error while trying to create a test object', function (done) {
            var testObject =
            {
                name: "testObjectFromUnitTest1",
                description: "object created from unit test"
            };

            request(url)
                .post('')
                .send(testObject)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(400);
                    done();
                });
        });

        it('Should successfully query a single test object with id 123456789012345678901234', function (done) {
            request(url)
                .get('/123456789012345678901234')
                .auth('testuser', 'password')
                .send()
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    res.body[0]._id.should.be.equal("123456789012345678901234");
                    done();
                });
        });

        it('Should return 500 error query an empty set with invalid ID', function (done) {
            request(url)
                .get('/xxx')
                .auth('testuser', 'password')
                .send()
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(500);
                    done();
                });
        });

        it('Should successfully query all test object', function (done) {
            request(url)
                .get('')
                .send()
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });

        it('Should delete all test objects', function (done) {
            var testObject = {
                name: {$regex: "testObjectFromUnitTest*"}
            }

            request(url)
                .delete('')
                .send(testObject)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });

        it('Should return [] for query on test objects', function (done) {
            var testObject = {
                name: "testObjectFromUnitTest",
                description: "object created from unit test"
            }

            request(url)
                .post('/byCriteria')
                .send(testObject)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });

});