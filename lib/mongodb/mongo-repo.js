var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');
var config = require('config');
//var dbConfig = config.get('database');
var url = "mongodb://localhost:27017/test"
var _ = require('lodash');

function createProperIDForObjectsWithID(objects) {
    _.map(objects, function (object) {
        if(object._id) {
            object._id = new ObjectID(object._id);
        }
    })
}

exports.createObject = function (collectionName, objects, callback) {
    createProperIDForObjectsWithID(objects);
    mongoInsertManyToCollection(collectionName, objects, callback);
};

exports.findByCriteria = function (collectionName, criteria, callback) {
    var options = {"sort": "name"};
    mongoFindByCriteria(collectionName, criteria, options, callback);
};

exports.findObjectById = function (id, collectionName, callback) {
    var o_id = new ObjectID(id);
    var query = {"_id" : o_id};
    var options = {"sort": "name"};
    mongoFindByCriteria(collectionName, query, options, callback);
};

exports.findAllInCollection = function (collectionName, callback) {
    var options = {"sort":[["name","desc"]]};
    mongoFindByCriteria(collectionName, {}, options, callback);
};

exports.deleteAllInCollection = function (collectionName, criteria, callback) {
    mongoDeleteByCriteria(collectionName, criteria, callback);
};

function processResultsToArray(db, result, collectionName, callback) {
    result.toArray((err, docs) => {
        if (err == null) {
            console.log("Found documents in the " + collectionName + " collection");
        } else {
            console.log("Failed to find documents in the " + collectionName + " collection");
        }
        callback(err, docs);
        db.close();
    })
}

function mongoFindByCriteria(collectionName, criteria, options, callback) {
    MongoClient.connect(url, function (err, db) {
        if (!err) {
            console.log("We are connected");
            var collection = db.collection(collectionName);
            collection.find(criteria, options, function (err, result) {
                processResultsToArray(db, result, collectionName, callback);
            });
        }
    });
}

function mongoDeleteByCriteria(collectionName, criteria, callback) {
    MongoClient.connect(url, function (err, db) {
        if (!err) {
            console.log("We are connected");
            var collection = db.collection(collectionName);
            collection.removeMany(criteria, function (err, result) {
                callback(err, result);
                db.close();
            });
        }
    });
};

function mongoInsertManyToCollection(collectionName, objects, callback) {
    MongoClient.connect(url, function (err, db) {
        if (!err) {
            console.log("We are connected");
            var collection = db.collection(collectionName);
            collection.insertMany(objects, function (err, result) {
                if (err == null) {
                    console.log("Successfully inserted a document into the " + collectionName + "collection.");
                } else {
                    console.log("Failed to insert a document into the " + collectionName + "collection.");
                }
                callback(err, result);
                db.close();
            });
        }
    });
}
