module.exports = {

    "createMenuItem" : function() {
        var menuItem = {};
        menuItem.name = "";
        menuItem.description = "";
        return menuItem;
    },
    "createTestObject" : function(data) {
        var testObject = {};
        testObject.name = data.name;
        testObject.description = data.description;
        return testObject;
    }

}