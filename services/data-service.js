var express = require('express');
var router = express.Router();
var mongoRepo = require('../lib/mongodb/mongo-repo.js');
var auth = require('../lib/auth/auth-helper.js')
/**
 * @api {get} /:collection/:id Get Object by ID
 * @apiName findObjectById
 * @apiGroup Models
 * @apiDescription Use this API to find an object by its ObjectID. The value of the ID
 * is what is stored in the NOSQL Database. If you want to search by a criteria see "Get Object by Criteria"
 *
 * @apiParam {String} collection Name of the collection you wish to access
 * @apiParam {String} id ObjectID of the desired object in collection
 *
 * @apiSuccessExample Success-Response:
 *  [
 *       {
 *        "_id": "56e620847c34c134516b00bc",
 *        "name": "testObjectFromUnitTest5",
 *        "description": "object created from unit test!!!"
 *       }
 *  ]
 *
 */
router.get('/:collection/:id', auth, function (req, res) {
    var id = req.params.id;
    var collection = req.params.collection;
    mongoRepo.findObjectById(id, collection, function (err, result) {
        if (err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

/**
 * @api {post} /:collection/byCriteria Get Objects by Criteria
 * @apiName findByCriteria
 * @apiGroup Models
 * @apiDescription This API will search all objects within a collection for those which fit the given criteria. The
 * criteria is a json object passed in the body.
 *
 * @apiParam {String} collection Name of the collection you wish to access
 *
 * @apiParamExample {json} Request-Example with Example:
 * {
 *  "name": "testObjectFromUnitTest5"
 * }
 * @apiSuccessExample Success-Response:
 *  [
 *       {
 *        "_id": "56e620847c34c134516b00bc",
 *        "name": "testObjectFromUnitTest5",
 *        "description": "object created from unit test!!!"
 *       }
 *  ]
 *
 */
router.post('/:collection/byCriteria', function (req, res) {
    var criteria = req.body;
    var collection = req.params.collection;
    if(Array.isArray(criteria)) {
        res.status(400).send({error: 'This service accepts an object, not an array.'});
    } else {
        mongoRepo.findByCriteria(collection, criteria, function (err, result) {
            if (err == null) {
                res.send(result);
            } else {
                res.send(err);
            }
        })
    }
});

/**
 * @api {get} /:collection Get All Objects of the collection
 * @apiName findAllInCollection
 * @apiGroup Models
 * @apiDescription This API will retrieve all objects in the collection
 *
 * @apiParam {String} collection Name of the collection you wish to access
 *
 * @apiSuccessExample Success-Response:
 *  [
 *       {
 *        "_id": "56e620847c34c134516b00bc",
 *        "name": "testObjectFromUnitTest5",
 *        "description": "object created from unit test!!!"
 *       }
 *  ]
 *
 */
router.get('/:collection', function (req, res) {
    var collection = req.params.collection;
    mongoRepo.findAllInCollection(collection, function (err, result) {
        if (err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

/*
 This delete endpoint does not take an array, accepts an object to use as a criteria for deletion
 {
 name: {$regex: "testObjectFromUnitTest*"},
 description: "object created from unit test"
 }
 */
/**
 * @api {delete} /:collection This will perform a delete based on a criteria
 * @apiName deleteAllInCollection
 * @apiGroup Models
 * @apiDescription This API will delete objects which match a criteria object see example below, this will delete
 * objects with similar name as testObjectFromUnitTest5
 *
 * @apiParam {String} collection Name of the collection you wish to access
 *
 * @apiParamExample {json} Request-Example:
 *  {
 *      "name": "testObjectFromUnitTest5"
 *  }
 *
 * @apiSuccessExample Success-Response:
 *  {
 *      "ok": 1,
 *      "n": 0
 *  }
 *
 */
router.delete('/:collection', function (req, res) {
    var criteria = req.body;
    var collection = req.params.collection;
    if (Array.isArray(criteria)) {
        res.status(400).send({error: 'This service accepts an object, not an array.'});
    } else {
        mongoRepo.deleteAllInCollection(collection, criteria, function (err, result) {
            if (err == null) {
                res.send(result);
            } else {
                res.send(err);
            }
        });
    }
});

/**
 * @api {post} /:collection Post an array of objects to collection
 * @apiName createObject(s)
 * @apiGroup Models
 * @apiDescription This service accepts an array of objects in json and persists them into the datastore
 *
 * @apiParam {String} collection Name of the collection you wish to access
 *
 * @apiParamExample {json} Request-Example with Example:
 *  [
 *       {
 *        "_id": "56e620847c34c134516b00bc",
 *        "name": "testObjectFromUnitTest5",
 *        "description": "object created from unit test!!!"
 *       }
 *  ]
 * @apiSuccessExample Success-Response:
 *  {
 *      "ok": 1,
 *      "n": 1
 *  }
 *
 */
router.post('/:collection', function (req, res) {
    var collection = req.params.collection;
    var object = req.body;

    if (!Array.isArray(object)) {
        res.status(400).send({error: "Input must be in an array."});
    } else {
        mongoRepo.createObject(collection, object, function (err, result) {
            if (err == null) {
                res.send(result);
            } else {
                res.send(err);
            }
        });
    }
});

module.exports = router;