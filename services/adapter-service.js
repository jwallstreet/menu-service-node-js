/**
 * This module is used to define services for adapter code, ingestion of data via file upload.
 */
var express = require('express');
var router = express.Router();
var mongoRepo = require('../lib/mongodb/mongo-repo.js');
var auth = require('../lib/auth/auth-helper.js')

router.get('/ping', auth, function (req, res) {
    res.send("adapter service is up and running!");
});

router.post('/upload', auth, function (req, res) {
    res.send("");
});

module.exports = router;