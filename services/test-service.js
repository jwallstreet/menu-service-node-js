var express = require('express');
var router = express.Router();
var mongoRepo = require('../lib/mongodb/mongo-repo.js');
var modelTemplates = require('../models/modelTemplates.js');
var config = require('config');
var collectionConfig = config.get('collection');

router.get('/testObjects/:id', function(req, res) {
    var id = req.params.id;
    mongoRepo.findObjectById(id, collectionConfig.test, function(err, result) {
        if(err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

router.post('/testObjects/byCriteria', function(req, res) {
    var criteria = req.body;
    mongoRepo.findByCriteria(collectionConfig.test, criteria, function(err, result) {
        if(err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

router.get('/testObjects', function(req, res) {
    mongoRepo.findAllInCollection(collectionConfig.test, function(err, result) {
        if(err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

router.delete('/testObjects', function(req, res) {
    var criteria = req.body;
    mongoRepo.deleteAllInCollection(collectionConfig.test, function(err, result) {
        if(err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

router.post('/testObjects', function(req, res) {
    var testObject = new modelTemplates.createTestObject(req.body);
    mongoRepo.createObject(collectionConfig.test, testObject, function(err, result) {
        if(err == null) {
            res.send(result);
        } else {
            res.send(err);
        }
    })
});

module.exports = router;